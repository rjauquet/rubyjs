square = (x) -> x * x
cube   = (x) -> square(x) * x

fill = (container, liquid = "coffee") -> "Filling the #{container} with #{liquid}..."

song = ["do", "re", "mi", "fa", "so"]

singers = {Jagger: "Rock", Elvis: "Roll"}

bitlist = [
  1, 0, 1
  0, 0, 1
  1, 1, 0
]

kids =
  brother:
    name: "Max"
    age:  11
  sister:
    name: "Ida"
    age:  9


outer = 1
person = (first) ->
  last = "Jauquet"
  self =
    name: first
    print: -> console.log(first + " " + last)
  return self

rob = person "Rob"

rob = person "Robb" if 2 == 3

isNum = false

me = if isNum then 4 else "four"
rob.print()

console.log(me)


#splats
gold = silver = rest = "unknown"

awardMedals = (first, second, others...) ->
  gold   = first
  silver = second
  rest   = others

contenders = [
  "Michael Phelps"
  "Liu Xiang"
  "Yao Ming"
  "Allyson Felix"
  "Shawn Johnson"
  "Roman Sebrle"
  "Guo Jingjing"
  "Tyson Gay"
  "Asafa Powell"
  "Usain Bolt"
]

awardMedals contenders...

console.log(rest)

eat = (food) -> console.log("eat " + food + "!")

eat food for food in ['toast', 'cheese', 'wine']

# Fine five course dining.
courses = ['greens', 'caviar', 'truffles', 'roast', 'cake']
eat courses[i], dish for dish, i in courses

# Health conscious meal.
foods = ['broccoli', 'spinach', 'chocolate']
eat food for food in foods when food isnt 'chocolate'

countdown = (num for num in [10..1])

console.log(countdown)

count = (food for food in foods)

console.log(count)

evens = (x for x in [0..10] by 4)

console.log(evens)


yearsOld = max: 10, ida: 9, tim: 11
ages = for child, age of yearsOld
  "#{child} is #{age}"

for child in ages
  console.log(child)

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

start   = numbers[0..2]

middle  = numbers[3...-2]

end     = numbers[-2..]

copy    = numbers[..]

console.log(start,middle,end,copy)

theBait   = 1000
theSwitch = 0

[theBait, theSwitch] = [theSwitch, theBait]

console.log(theBait, theSwitch)

tag = "<impossible>"

[open, contents..., close] = tag.split("")

futurists =
  sculptor: "Umberto Boccioni"
  painter:  "Vladimir Burliuk"
  poet:
    name:   "F.T. Marinetti"
    address: [
      "Via Roma 42R"
      "Bellagio, Italy 22021"
    ]

{poet: {name, address: [street, city]}} = futurists

console.log(futurists)

text = "Every literary critic believes he will
        outwit history and have the last word"

[first, ..., last] = text.split " "

console.log(first,last)





















console.log('end')
