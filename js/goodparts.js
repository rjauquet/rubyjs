/*
    poor object notation
*/

function add(){
    this.i = 0;
    var inc = function(){
            i++;
    };
    var geti = function(){
            return i;
    };
}

/*
    correct object notation
*/

var obj = function (){
    var i = 0;
    return {
        inc: function(){
            i++;
        },
        geti: function(){
            return i;
        }
    };
};

/*
    removes the need to type prototype to add methods to a defined type
*/

Function.prototype.method = function (name, func) {
    this.prototype[name] = func;
    return this;
};

/*
    adds currying to JavaScript!
*/

Function.method('curry', function (  ) {
    var slice = Array.prototype.slice,
        args = slice.apply(arguments),
        that = this;
    return function (  ) {
        return that.apply(null, args.concat(slice.apply(arguments)));
    };
});

/*
    memoization reduces computation power by storing precomputed results in a closure
*/

var fibonacci = function (  ) {
    var memo = [0, 1];
    var fib = function (n) {
        var result = memo[n];
        if (typeof result !== 'number') {
            result = fib(n - 1) + fib(n - 2);
            memo[n] = result;
        }
        return result;
    };
    return fib;
};

/*
    makes a memoizer from a given function and inital memo array
*/

var memoizer = function (memo, fundamental) {
    var shell = function (n) {
        var result = memo[n];
        if (typeof result !== 'number') {
            result = fundamental(shell, n);
            memo[n] = result;
        }
        return result;
    };
    return shell;
};

//fib memoes
var fibonacci = memoizer([0, 1], function (shell, n) {
    return shell(n - 1) + shell(n - 2);
});

//fib factorial
var factorial = memoizer([1, 1], function (shell, n) {
    return n * shell(n - 1);
});


//object inherits

Function.method('inherits', function (Parent) {
    this.prototype = new Parent(  );
    return this;
});


/*
    construtor notation - pass an object


var myObject = maker({
    first: 'f',
    last: 'l',
    state: 's',
    city: 'c'
});

*/

/*
pseudocode object template

var constructor = function (spec, my) {
    var that, other private instance variables;
    my = my || {};

    Add shared variables and functions to my that = a new object;
    Add privileged methods to that

    return that;
}

two steps for adding a function to an object


var methodical = function (  ) {
    ...
};
that.methodical = methodical;

ex:
*/
var mammal = function (spec) {
    var that = {};
    that.get_name = function (  ) {
        return spec.name;
    };
    that.says = function (  ) {
        return spec.saying || '';
    };
    return that;
};
var myMammal = mammal({name: 'Herb'});

//then we can inherit like this:

var cat = function (spec) {

    spec.saying = spec.saying || 'meow';
    var that = mammal(spec);
    that.purr = function (n) {
        var i, s = '';
        for (i = 0; i < n; i += 1) {
            if (s) {
                s += '-';
            }
            s += 'r';
        }
        return s;
    };
    that.get_name = function () {
        return that.says() + ' ' + spec.name + ' ' + that.says(  );
    };
    return that;
};
var myCat = cat({name: 'Henrietta'});


//define super methods for objects

Object.method('superior', function (name) {
    var that = this,
        method = that[name];
    return function (  ) {
        return method.apply(that, arguments);
    };
});

//ex:
var coolcat = function (spec) {
    var that = cat(spec),
        super_get_name = that.superior('get_name');
    that.get_name = function (n) {
        return 'like ' + super_get_name(  ) + ' baby';
    };
    return that;
};




var is_array = function (value) {
    return value &&
        typeof value === 'object' &&
        typeof value.length === 'number' &&
        typeof value.splice === 'function' &&
        !(value.propertyIsEnumerable('length'));
};


String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};


Array.method('reduce', function (f, value) {
    var i;
    for (i = 0; i < this.length; i += 1) {
        value = f(this[i], value);
}
    return value;
});


Array.dim = function (dimension, initial) {
    var a = [], i;
    for (i = 0; i < dimension; i += 1) {
        a[i] = initial;
    }
    return a;
};

// Make an array containing 10 zeros.
var myArray = Array.dim(10, 0);


Array.matrix = function (m, n, initial) {
    var a, i, j, mat = [];
    for (i = 0; i < m; i += 1) {
        a = [];
        for (j = 0; j < n; j += 1) {
            a[j] = 0;
        }
        mat[i] = a;
    }
    return mat;
};

// Make a 4 * 4 matrix filled with zeros.
var myMatrix = Array.matrix(4, 4, 0);




//n-dim arrays
function createNDimArray(dimensions) {
    if (dimensions.length > 0) {
        var dim = dimensions[0];
        var rest = dimensions.slice(1);
        var newArray = [];
        for (i = 0; i < dim; i++) {
            newArray[i] = createNDimArray(rest);
        }
        return newArray;
     }
     else {
        return undefined;
     }
 }

//get element in n-dim array
function getElement(array, indices) {
    if (indices === 0) {
        return array;
    }
    else {
        return getElement(array[indices[0]], indices.slice(1));
    }
}


console.log(myMammal);
console.log(myCat);

var ob1 = obj();
var ob2 = obj();

ob1.inc();
ob1.inc();
console.log("i is ", ob1.geti());
