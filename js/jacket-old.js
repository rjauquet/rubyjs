#! /usr/local/bin/node

/** ** ** ** ** ** **
 ** ** ** ** ** ** **
 **
 ** Author: Rob Jauquet
 **
 **
 **
 **
 ** ** ** ** ** ** **
 ** ** ** ** ** ** **/
//default setup for Rob Jauquet node applications

var sys = require('sys');
var fs = require('fs');
var exec = require('child_process').exec;

var child;
function puts(error, stdout, stderr) {
    sys.puts(stdout);
}

function printRoot(root){
    printFolder("",root,0);
}

function printFiles(prefix,files){
    if(files != null){
        if(files.length > 0){
            for(var i=0; i<files.length; i++){
                printFile(prefix, files[i]);
            }
        }
        else{
            console.log(prefix + "<empty>");
        }
    }
}

function printFile(prefix,file){

//    console.log("file",file);

    if(file.name != null){
        console.log(prefix + file.name);
    }
    else{
        console.log(prefix + " no filename");
    }
    if(file.location != null){
        console.log(prefix + "location: " + file.location);
    }
}

function printFolders(prefix,folders,level){
    var tab = "";
    for(var i=0; i<level; i++){
        tab += "  ";
    }
    for(var j=0; j<folders.length; j++){
        printFolder(tab, folders[j],level);
    }
}

function printFolder(prefix,folder,level){

//    console.log("folder",folder);

    //print name of folder
    if(folder.name != null){
        console.log(prefix,folder.name);
    }
    else{
        console.log(prefix,"no folder name");
    }

    //print files
    if(folder.files != null){
        printFiles(prefix + " - ", folder.files);
    }
    //print folders
    if(folder.folders != null){
        printFolders(prefix, folder.folders,level+1);
    }
    else{
        console.log(prefix,"<empty>");
    }
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};


var args = process.argv;
// print process.argv
process.argv.forEach(function (val, index, array) {
  console.log(index + ': ' + val);
});


/*
    The args should specify the location of a json object and optionally the location
    of the root for the creation of the jacket.

    jacket <file loc>
    jacket <file loc> <loc>

    the json will specify folder structure and files, as well as optional information
    for version control.

    valid nodes for now will be file and folder

    ex:
*/


//looks for a json file in the current directory, prints error if it doesn't exist
//check if not null
//check if ends with .json
if(args[2] !== null){
    var parse = JSON.parse(fs.readFileSync(args[2], 'utf8'));
    printRoot(parse);
}


/*
for(var i = 2; i<args.length; i++){
    if(args[i].endsWith(".js")){
        exec("touch " + args[i], puts);
    }
    else{
        exec("mkdir " + args[i], puts);
    }
}
*/


