#! /usr/local/bin/node

/** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **
 ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **
 **
 ** Author:
 **     Rob Jauquet
 **
 ** Installation:
 **     install node.js
 **     add jacket.js to /usr/local/bin/
 **     edit ~/.bash_profile and add 'alias'
 **
 **
 **
 **
 **
 **
 **
 **
 **
 **
 **
 ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **
 ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **/

var jacket = function(){
    var sys = require('sys');
    var fs = require('fs');
    //async
    //var exec = require('child_process').exec;
    //sync
    var shell = require('shelljs');
    var args = process.argv;
    var self = {
        printArgs: function(){
            process.argv.forEach(function (val, index, array) {
                console.log(index + ': ' + val);
            });
        },
        printUsage: function(){
            console.log("     usage: jacket <path to json>");
            console.log("");
            console.log("     From the directory that will contain the project,");
            console.log("     run jacket while providing a well-formed json");
            console.log("     description. See repo for examples.");
        },
        run: function(){
            try{
                var parse = JSON.parse(fs.readFileSync(args[2], 'utf8'));
                self.printRoot(parse);
            }
            catch(err){
                //if there are more than two args, print error, otherwise just print usage
                if(args.length > 2){
                    console.log(err);
                    self.printArgs();
                }
                self.printUsage();
            }
        },
        callback: function (error, stdout, stderr) {
            //console.log("error",error);
            //console.log("stdout",stdout);
            //console.log("stderr",stderr);
        },
        printRoot: function (root){
            self.printFolder("",root,0);
        },
        printFiles: function (prefix,files){
            if(files != null){
                if(files.length > 0){
                    for(var i=0; i<files.length; i++){
                        self.printFile(prefix, files[i]);
                    }
                }
                else{
                    console.log(prefix.substring(0,prefix.length-2) + "  <empty>");
                }
            }
        },
        printFile: function (prefix,file){

        //    console.log("file",file);

            if(file.name !== null){
                console.log(prefix + file.name);
                //exec("touch " + file.name, self.callback);
                shell.exec("touch " + file.name);
            }
            else{
                console.log(prefix + " no filename");
            }
            if(file.location !== null){
                console.log(prefix + "location: " + file.location);
            }
        },
        printFolders: function (prefix,folders,level){
            var tab = "";
            for(var i=0; i<level; i++){
                tab += "  ";
            }
            for(var j=0; j<folders.length; j++){
                self.printFolder(tab, folders[j],level);
                //exec("cd ..", self.puts);
                shell.cd("..");
            }
        },
        printFolder: function (prefix,folder,level){

        //    console.log("folder",folder);

            //print name of folder
            if(folder.name !== null){
                console.log(prefix,folder.name);
                //exec("mkdir " + folder.name, function(){
                //    exec("cd " + folder.name, self.callback);
                //});
                shell.mkdir('-p',folder.name);
                shell.cd(folder.name);
            }
            else{
                console.log(prefix,"no folder name");
            }

            //print files
            if(folder.files !== null){
                self.printFiles(prefix + " - ", folder.files);
            }
            //print folders
            if(folder.folders !== null){
                self.printFolders(prefix, folder.folders,level+1);
            }
            else{
                console.log(prefix,"<empty>");
            }
        }
    };
    return self;
};

jacket().run();
