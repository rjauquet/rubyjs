#! /usr/local/bin/node

var fs = require('fs');
var exec = require('child_process').exec;
var args = process.argv;


/*

when someone runs jacket ~/path/file.json

    1. runalias /usr/local/bin/jacket.js ~/path/file.json
    2. node /usr/local/bin/runalias.js /usr/local/bin/jacket.js ~/path/file.json


alias abspath='/usr/local/bin/apath'
alias runalias='node /usr/local/bin/runalias.js'
alias jacket='runalias /usr/local/bin/jacket.js'

*/

try{
    exec("/usr/local/bin/apath " + args[2], function(error, stdout, stderr){
        exec(stdout.substring(0,stdout.length-1) + " " + args.slice(3), function(error, stdout, stderr){
            console.log(stdout);
        });
    });
}
catch(err){
    console.log(err);
    return null;
}

function stringArgs(arr){
    var str = "";
    for(var i=0; i<arr.length; i++){
        str += arr[i] + " ";
    }
    console.log("str",str);
    return str;
}