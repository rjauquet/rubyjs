var compressor = require('node-minify');

// Compress javascript with advanced closure compiler
new compressor.minify({
    type: 'gcc',
    fileIn: 'js/*.js',
    fileOut: 'js/min/ruby.min.js',
    options: ['--compilation_level=ADVANCED_OPTIMIZATIONS'],
    callback: function(err, min){
        console.log(err);
    }
});
